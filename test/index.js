const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../dist/index.js');

//Assertion style
chai.should();

chai.use(chaiHttp);

describe('Persons API', () => {

    /**
     * Test the GET ALL route
     */
    describe("GET /persons", () => {
        it("It should GET all the persons", (done) => {
            chai.request(server).get("/persons").end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('array');
                done();
            });
        });

        it("It should not GET all the persons", (done) => {
            chai.request(server).get("/person").end((err, response) => {
                response.should.have.status(404);
                done();
            });
        });
    });

    /**
     * Test the GET route
     */
    describe("GET /persons/:id", () => {
        it("It should GET an array of person", (done) => {
            chai.request(server).get("/persons/1").end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('array');
                done();
            });
        });
    });

    /**
    * Test the POST route
    */
    describe("POST /persons", () => {
        it("It should not POST a person", (done) => {
            chai.request(server).post("/persons").send().end((err, response) => {
                response.should.have.status(500);
                done();
            });
        });
    });

    /**
    * Test the PUT route
    */
    describe("PUT /persons", () => {
        it("It should not PUT a person", (done) => {
            chai.request(server).put("/persons/1").send().end((err, response) => {
                response.should.have.status(500);
                done();
            });
        });
    });

    /**
    * Test the DELETE route
    */
   describe("PUT /persons", () => {
    it("It should not DELETE persons", (done) => {
        chai.request(server).delete("/persons").send().end((err, response) => {
            response.should.have.status(404);
            done();
        });
    });
});

});
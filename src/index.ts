import express, { Application} from 'express';
import indexRoutes from './routes/index';
import morgan from 'morgan';
const app: Application = express();

// middlewares
app.use(morgan('tiny'))
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Routes
app.use(indexRoutes);

app.listen(3000, () => {
    console.log('Server on port', 3000);
});

export = app;
import {Router} from 'express';
const router = Router();

import { getPersons, getPersonById, createPerson, updatePerson, deletePerson } from '../controllers/index.controller';

router.get('/persons', getPersons);
router.get('/persons/:id', getPersonById);
router.post('/persons', createPerson);
router.put('/persons/:id', updatePerson)
router.delete('/persons/:id', deletePerson);

export default router;
import { Request, Response } from 'express';
import { pool } from '../database';
import { QueryResult } from 'pg';

export const getPersons = async (req: Request, res: Response): Promise<Response> => {
    try {
        const response: QueryResult = await
            pool.query('SELECT * FROM persons ORDER BY id ASC');
        return res.status(200).json(response.rows);
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
};

export const getPersonById = async (req: Request, res: Response): Promise<Response> => {
    try {
        const id = parseInt(req.params.id);
        const response: QueryResult = await pool.query('SELECT * FROM persons WHERE id = $1', [id]);
        return res.json(response.rows);
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
};

export const createPerson = async (req: Request, res: Response) => {
    try {
        const { fullname, birth, father_id, mother_id } = req.body;
        const response = await pool.query('INSERT INTO persons (fullname, birth, father_id, mother_id) VALUES ($1, $2)', [fullname, birth, father_id, mother_id]);

        res.json({
            message: 'Person Added successfully',
            data: response.rows
        });
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
};

export const updatePerson = async (req: Request, res: Response) => {
    try {
        const id = parseInt(req.params.id);
        const { fullname, birth, father_id, mother_id } = req.body;

        const response = await pool.query('UPDATE persons SET name = $1, email = $2, father_id = $3, mother_id = $4 WHERE id = $5', [
            fullname, birth, father_id, mother_id,
            id
        ]);
        res.json({
            message: 'Person Updated successfully',
            data: response.rows
        });
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
};

export const deletePerson = async (req: Request, res: Response) => {
    try {
        const id = parseInt(req.params.id);
        await pool.query('DELETE FROM persons where id = $1', [
            id
        ]);
        res.json(`Person ${id} deleted Successfully`);
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
};
CREATE DATABASE bdbdatabase;

CREATE TABLE persons(
    id SERIAL PRIMARY KEY,
    fullname TEXT,
    birth DATE,
    father_id INT,
    mother_id INT
)
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deletePerson = exports.updatePerson = exports.createPerson = exports.getPersonById = exports.getPersons = void 0;
const database_1 = require("../database");
exports.getPersons = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield database_1.pool.query('SELECT * FROM persons ORDER BY id ASC');
        return res.status(200).json(response.rows);
    }
    catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
});
exports.getPersonById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const response = yield database_1.pool.query('SELECT * FROM persons WHERE id = $1', [id]);
        return res.json(response.rows);
    }
    catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
});
exports.createPerson = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { fullname, birth, father_id, mother_id } = req.body;
        const response = yield database_1.pool.query('INSERT INTO persons (fullname, birth, father_id, mother_id) VALUES ($1, $2)', [fullname, birth, father_id, mother_id]);
        res.json({
            message: 'Person Added successfully',
            data: response.rows
        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
});
exports.updatePerson = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const { fullname, birth, father_id, mother_id } = req.body;
        const response = yield database_1.pool.query('UPDATE persons SET name = $1, email = $2, father_id = $3, mother_id = $4 WHERE id = $5', [
            fullname, birth, father_id, mother_id,
            id
        ]);
        res.json({
            message: 'Person Updated successfully',
            data: response.rows
        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
});
exports.deletePerson = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        yield database_1.pool.query('DELETE FROM persons where id = $1', [
            id
        ]);
        res.json(`Person ${id} deleted Successfully`);
    }
    catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../dist/index.js");
//Assertion style
chai.should();
chai.use(chaiHttp);
describe('Persons API', () => {
    /**
     * Test the GET ALL route
     */
    describe("GET /persons", () => {
        it("It should GET all the persons", (done) => {
            chai.request(server).get("/persons").end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('array');
                done();
            });
        });
        it("It should not GET all the persons", (done) => {
            chai.request(server).get("/person").end((err, response) => {
                response.should.have.status(404);
                done();
            });
        });
    });
    /**
     * Test the GET route
     */
    describe("GET /persons/:id", () => {
        it("It should GET an array of user", (done) => {
            chai.request(server).get("/persons/1").end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('array');
                done();
            });
        });
    });
    /**
    * Test the POST route
    */
    /**
    * Test the PUT route
    */
    /**
    * Test the PATCH route
    */
    /**
    * Test the DELETE route
    */
});
